from Move_player_men import Move_player_men

class Taking_player_men(Move_player_men):
    def __init__(self):
        super().__init__()

    def make_list_possible_move_checker_1(self):
        xy_list = []
        for i in range(0, 8):
            for j in range(65, 73):
                val_in = super().read_xy_val(i, j)
                if val_in == 1:
                    for k in range(0, 8):
                        for m in range(65, 73):
                            val_out = super().read_xy_val(k, m)
                            if val_out == 3:
                                xy_list.append([i, j, k, m])
        return xy_list

    def make_list_possible_move_king_1(self):
        xy_list = []
        for i in range(0, 8):
            for j in range(65, 73):
                val_in = super().read_xy_val(i, j)
                if val_in == 11:
                    for k in range(0, 8):
                        for m in range(65, 73):
                            val_out = super().read_xy_val(k, m)
                            if val_out == 3:
                                xy_list.append([i, j, k, m])
        return xy_list

    def make_xy_list_possible_move_1(self):
        xy_possible_move_lists = []
        a = self.make_list_possible_move_checker_1()
        b = self.make_list_possible_move_king_1()

        move_lists = super().make_list_of_possible_move_all_chacker(a, b)
        #print(move_lists)
        for i in range(len(move_lists)):
          c = move_lists[i]
          super().set_trigger_1(c[0], c[1], c[2], c[3])
          if self.trigger_1:
                xy_possible_move_lists.append(move_lists[i])
                self.trigger_1 = False
        #print(xy_possible_move_lists)
        return xy_possible_move_lists

    def cal_max_punch_1(self):
        max_punch_list =[]
        xy_possible_move_lists = self.make_xy_list_possible_move_1()
        for i in range(len(xy_possible_move_lists)):
             a = xy_possible_move_lists[i]
             b = super().set_val_from_in_to_out(a[0], a[1], a[2], a[3])
             c = b[1:-1]
             max_punch_list.append(c.count(2) + c.count(22))
        if len(max_punch_list) > 0:
             max_punch_val = max(max_punch_list)
        else:
             max_punch_val = 0
        #print(f"max_punch_val: {max_punch_val}")
        return max_punch_val

# punch = Punch_1()
# b = punch.cal_max_punch_1()
# print(b)