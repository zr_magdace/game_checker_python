from Move_players import Move_players
import random

class Move_player_AI(Move_players):
    def __init__(self):
        super().__init__()
        self.xy_in_2 = None
        self.xy_out_2 = None
        self.trigger_2 = False
        self.punch_2 = 0
        self.n_checker_1 = 12

    def cal_val_punch_2(self, x_in, y_in, x_out, y_out):
        parametere_list = super().set_parameter_list(x_in, y_in, x_out, y_out)  # [delta, delta_x, val_list]
        var_list = parametere_list[2]
        self.triger_2 = self.set_trigger_2(x_in, y_in, x_out, y_out)
        if self.trigger_2 == True:
           self.punch_2 = var_list.count(11) + var_list.count(1)
        else:
           self.punch_2 = -1
        #print(self.punch_2, var_list )
        self.trigger_2 = False
        return self.punch_2

    def set_trigger_2(self, x_in, y_in, x_out, y_out):  #metoda słuzaca do sprawdzenia czy ruch jest zgodny z zasadamik
        parametere_list = super().set_parameter_list(x_in, y_in, x_out, y_out)  # [delta, delta_x, val_list]
        var_list = parametere_list[2]
        #print(f"var_list do trigger {var_list}")
        if (len(var_list) > 0):
            a = var_list[-1]  # ostatnia wartosc na liscie
            aa = var_list[0]
            b = any(var_list[i] == var_list[i + 1] and var_list[i] in [1, 11] for i in range(len(var_list) - 1))
            bb = any(var_list[i] == 1 and var_list[i+1] == 11 for i in range(len(var_list) - 1))
            bbb = any(var_list[i] == 11 and var_list[i + 1] == 1 for i in range(len(var_list) - 1))
            c = 1 in var_list
            cc = 11 in var_list
            d = 2 in var_list[1:]
            e = 22 in var_list[1:]
            f = -1 in var_list
            g = 3 in var_list[:-1]

            # warunki  opisujace ruch zwyklego pionka z biciem
            if (parametere_list[0] == 1 and parametere_list[1] == -2 and aa == 2  and a == 3 and b == False
                and (c == True or cc == True) and d == False and e == False and f == False and g == False):
                self.trigger_2 = True
            # warunki  opisujace ruch zwyklego pionka bez bicia
            elif (parametere_list[0] == 1 and parametere_list[1] == -1 and aa == 2 and a == 3):
                self.trigger_2 = True
            # warunki  opisujace ruch damki
            elif (parametere_list[0] == 1  and parametere_list[1] != 0 and aa == 22 and a == 3 and
                    b == False and bb == False and bbb == False and d == False and e == False and f == False):
                self.trigger_2 = True
        # warunki do opisy ruch zwyklego pionka z dwoma biciami (po "luku")
        if len(var_list) == 5:
            k, l, m, n, p = var_list[:5]
            if (parametere_list[0] == 0 and parametere_list[1] == 0  and (l == 1 or l == 11) and
                    m == 3 and (n == 1 or n == 11) and p == 3):
                self.trigger_2 = True
        #print(self.trigger_2)
        return self.trigger_2

    def set_n_1(self, x_in, y_in, x_out, y_out):  # jezeli jest bicie usuniecie zbitego pionka i zmiana ilosci pionków
        val_list = super().set_val_from_in_to_out(x_in, y_in, x_out, y_out)
        #print(f"val_list z ser_n_1 {val_list}")
        xy_list = super().set_xy_from_in_to_out(x_in, y_in, x_out, y_out)
        #print(f"xy_list z ser_n_1 {xy_list}")
        for i in range(1, len(xy_list)):
            if self.trigger_2 == True and self.punch_2 > 0 and (val_list[i] == 1 or val_list[i] == 11):
               self.df_xy.at[xy_list[i][0], xy_list[i][1]] = 3
               self.n_checker_1 = self.n_checker_1 - 1
            else:
               self.n_checker_1 = self.n_checker_1
        return self.n_checker_1

    def print_n_checker_1(self, name):
        print(f"{name} ma  {self.n_checker_1} pionek/ow ")

    def change_xy_val_2(self, x_in, y_in, x_out, y_out): # zmiana pozycji pionka po wykonaniu ruchu
        a = self.read_xy_val(x_in, y_in)
        if a == 2:
            if (self.trigger_2 == True) and (x_out != 0):
                self.df_xy.at[x_in, y_in] = 3
                self.df_xy.at[x_out, y_out] = 2
            elif (self.trigger_2 == True) and (x_out == 0):
                self.df_xy.at[x_in, y_in] = 3
                self.df_xy.at[x_out, y_out] = 22
        if a == 22:
            self.df_xy.at[x_in, y_in] = 3
            self.df_xy.at[x_out, y_out] = 22

        return self.df_xy

    def move_2(self, punch_max, list_possible_move):
        a = random.randint(0, len(list_possible_move)-1)
        b = list_possible_move[a]
        #print(b)
        self.xy_in_2 = [b[0], b[1]]
        self.xy_out_2 = [b[2], b[3]]

        self.cal_val_punch_2(self.xy_in_2[0], self.xy_in_2[1], self.xy_out_2[0], self.xy_out_2[1])
        if (self.punch_2 == punch_max):
            f = self.set_trigger_2(self.xy_in_2[0], self.xy_in_2[1], self.xy_out_2[0], self.xy_out_2[1])
            f = self.set_n_1(self.xy_in_2[0], self.xy_in_2[1], self.xy_out_2[0], self.xy_out_2[1])
            #print(f" set_n1 {f}")
            self.change_xy_val_2(self.xy_in_2[0], self.xy_in_2[1], self.xy_out_2[0], self.xy_out_2[1])
        #print(f" self.punch_2: {self.punch_2}")
        return self.df_xy

# a = Move_2()
# b = a.set_parameter_list(5, 66, 4, 65)
# print (b)