from Move import Move
from Move_1 import Move_1
from Move_2 import Move_2
from Board import Board
from Punch_2 import Punch_2
from Punch_1 import Punch_1

import time

class Play():
    def __init__(self):
        self.board = Board()
        self.move = Move()
        self.move_player = Move_1()
        self.move_ai = Move_2()
        self.punch_1 = Punch_1()
        self.punch_2 = Punch_2()

    #a=move_player.cal_val_xy_between_in_out(0,65, 2, 67)
    #b=move.set_parameter_list(0,65, 2, 67)
    #print (f"test {b[2]}")
    def set_result(self):
        if self.move_ai.n_checker_1 == 0:
            print("Wygrał gracz 2: AI")
        elif self.move_player.n_checker_2 == 0:
            print(f"Wygrał gracz 1:{self.board.player}")
        else:
            print("Kolejna runda")

    def run_game(self):
        self.board.introduce()
        print("---------------")
        self.board.set_player()
        print("---------------")
        self.board.print_board_sign(self.move.df_xy)

        while self.move_player.n_checker_2 > 0 and self.move_ai.n_checker_1 > 0:
            self.move_player.trigger_1 = False
            self.move_ai.trigger_2 = False
            self.move_player.punch_1 = 0
            self.move_ai.punch_2 = 0

            print("--------------------")
            print(f"Gracz 1: {self.board.player} wykonuje ruch")
            print("-------------------- ")

            punch_max_1 = self.punch_1.cal_max_punch_1()
            self.move_player.trigger_1 = False

            while self.move_player.trigger_1 == False:
                self.move_player.move_1(punch_max_1)

            self.board.print_board_sign(self.move.df_xy)

            if self.move_player.n_checker_2 > 0:
                print("--------------------")
                print("Gracz 2: AI wykonuje ruch")
                print("--------------------")
                time.sleep(2)
                list_move = self.punch_2.make_xy_list_possible_move_2()
                self.punch_max_2 = self.punch_2.cal_max_punch_2(list_move)
                self.move_ai.trigger_2 = False

                while self.move_ai.trigger_2 == False:
                    self.move_ai.move_2(self.punch_max_2, list_move)

                self.board.print_board_sign(self.move.df_xy)
                self.move_player.print_n_checker_2()
                self.move_ai.print_n_checker_1(self.board.player)
                self.set_result()
