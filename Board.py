import colorama
class Board:
    def __init__(self):
        self.player = "Anonymus"

    def introduce(self):
        print("Rozpoczynasz grę w warcaby.\nTwoim przeciwnikiem jest AI. Pole oznaczone [1] to twoje pionki.\nPola oznaczone [2] to pionki przeciwnika.\nDamka jest oznaczona jako [D] i odpowiednim kolorem.\nPole puste oznaczone jest [o]. Pole zabronione do ruch [x].\nPOWODZENIA !!!")

    def set_player(self):
        self.player = input("Podaj swoje imie: ")
        return self.player
    def print_board_sign(self, df_xy):    #funkcja do testowania
        size = 8
        letter_int = ord('A')
        print(" ", end=" ")  # Etykiety kolumn (litery)
        for i in range(size):
            print(chr(letter_int + i), end="  ")
        print()
        for i in range(size):  # Etykieta wiersza (cyfra)
            print(i + 1, end=" ")
            for j in range(size):
                if df_xy.at[i, (letter_int + j)] == 0:
                    print("x", end="  ")
                if df_xy.at[i, (letter_int + j)] == 1:
                    print("\033[95m1\033[0m", end="  ")
                if df_xy.at[i, (letter_int + j)] == 2:
                    print("\033[32m2\033[0m", end="  ")
                if df_xy.at[i, (letter_int + j)] == 3:
                    print(("o"), end="  ")
                if df_xy.at[i, (letter_int + j)] == 11:
                    print("\033[95mD\033[0m", end="  ")
                if df_xy.at[i, (letter_int + j)] == 22:
                    print("\033[32mD\033[0m", end="  ")
            print()
    def print_board_number(self, df_xy):
        size = 8
        letter_int = ord('A')

        print(" ", end=" ")  # Etykiety kolumn (litery)
        for i in range(size):
            print(chr(letter_int + i), end=" ")
        print()
        for i in range(size):  # Etykieta wiersza (cyfra)
            print(i + 1, end=" ")
            for j in range(size):
                print(df_xy.at[i, (letter_int + j)], end=" ")
            print()
