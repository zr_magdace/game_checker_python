from Move_players import Move_players

class Move_player_men(Move_players):
    def __init__(self):
        super().__init__()
        self.xy_in_1 = None
        self.xy_out_1 = None
        self.trigger_1 = False
        self.punch_1 = 0
        self.n_checker_2 = 12

    def read_xy_in_1(self):
        Ln = input("Podaj pozycję pionka początkową [LiteraCyfra]: ")
        xy_list = super().read_xy(Ln)
        #print(xy_list)
        return xy_list

    def read_xy_out_1(self):
        Ln = input("Podaj pozycję pionka końcową: [LiteraCyfra]: ")
        xy_list = super().read_xy(Ln)
        #print(xy_list)
        return xy_list

    def cal_val_punch_1(self, x_in, y_in, x_out, y_out):
        parametere_list = super().set_parameter_list(x_in, y_in, x_out, y_out)  # [delta, delta_x, val_list]
        var_list = parametere_list[2]
        self.triger_1 = self.set_trigger_1(x_in, y_in, x_out, y_out)
        #print(self.trigger_1)
        if self.trigger_1 == True:
             self.punch_1 = var_list.count(22) + var_list.count(2)

        elif self.trigger_1 == False:
             print("Ruch niezgodny z zasadami. Podaj pozycje ponownie")
             self.punch_1 = -1
        #print(f" self.punch_1: {self.punch_1}")
        self.trigger_1 = False
        return self.punch_1

    def set_trigger_1(self, x_in, y_in, x_out, y_out):
        parametere_list = super().set_parameter_list(x_in, y_in, x_out, y_out)  # [delta, delta_x, val_list]
        #print(f"parametere_list dla danego xy out /in{parametere_list}")
        var_list = parametere_list[2]
        if len(var_list) > 0:
            aa = var_list[0]
            a = var_list[-1]  # ostatnia wartosc na liscie
            b = any(var_list[i] == var_list[i + 1] and var_list[i] in [2, 22] for i in range(len(var_list) - 1))
            bb = any(var_list[i] == 2 and var_list[i + 1] == 2 for i in range(len(var_list) - 1))
            bbb = any(var_list[i] == 22 and var_list[i + 1] == 2 for i in range(len(var_list) - 1))
            c = 2 in var_list[1:]
            cc = 22 in var_list[1:]
            d = 1 in var_list[1:]
            e = 11 in var_list[1:]
            f = -1 in var_list
            g = 3 in var_list[:-1]
            # warunki  opisujace ruch zwyklego pionka z biciem
            if (parametere_list[0] == 1 and parametere_list[1] == 2 and aa == 1
                    and a == 3 and b == False  and  (c== True or cc == True)  and d == False and
                    e == False and f == False and g == False):
                self.trigger_1 = True
            # warunki  opisujace ruch zwyklego pionka bezz bicia
            elif (parametere_list[0] == 1 and parametere_list[1] == 1 and aa == 1 and a == 3):
                self.trigger_1 = True
            # warunki  opisujace ruch damki
            elif (parametere_list[0] == 1 and parametere_list[0] != 0 and aa == 11 and a == 3 and
                    b == False  and bb == False and bbb == False and d == False and e == False and f == False):
                self.trigger_1 = True
        # warunki do opisy ruch zwyklego pionka z dwoma biciami (po "luku")
        if len(var_list) == 5:
            k, l, m, n, p = var_list[:5]   # parametry do opisy ruch zwyklego pionka po "luku"
            if (parametere_list[0] == 0 and parametere_list[1] == 0 and k == 1 and (l == 2 or l == 22) and
                    m == 3 and (n == 2 or n == 22) and p == 3):
                self.trigger_1 = True
                #print(parametere_list[0], parametere_list[1], var_list[0], k, a, b, c, d, e, f, g)
        return self.trigger_1

    def set_n_2(self, x_in, y_in, x_out, y_out):  # jezeli jest bicie usuniecie zbitego pionka i zmiana ilosci pionków
        val_list = super().set_val_from_in_to_out(x_in, y_in, x_out, y_out)
        #print(f"val_list z ser_n_1 {val_list}")
        xy_list = super().set_xy_from_in_to_out(x_in, y_in, x_out, y_out)
        #print(f"val_list z set_n_2: {xy_list}")
        for i in range(1, len(xy_list)):
            if  self.trigger_1 == True and self.punch_1 > 0 and (val_list[i] == 22 or val_list[i] == 2):
                self.df_xy.at[xy_list[i][0], xy_list[i][1]] = 3
                self.n_checker_2 = self.n_checker_2 - 1
            else:
                self.n_checker_2  = self.n_checker_2
        return  self.n_checker_2

    def print_n_checker_2(self):
        print(f"Gracz 2 / AI / ma {self.n_checker_2} pionek/ow ")

    def change_xy_val_1(self, x_in, y_in, x_out, y_out):
        a = self.read_xy_val(x_in, y_in)
        if a == 1:
            if self.trigger_1 == True and x_out != 7:
                self.df_xy.at[x_in, y_in] = 3
                self.df_xy.at[x_out, y_out] = 1
            elif self.trigger_1 == True and x_out == 7:
                self.df_xy.at[x_in, y_in] = 3
                self.df_xy.at[x_out, y_out] = 11
        if a == 11:
                self.df_xy.at[x_in, y_in] = 3
                self.df_xy.at[x_out, y_out] = 11

    def move_1(self, punch_max):
        self.xy_in_1 = self.read_xy_in_1()
        self.xy_out_1 = self.read_xy_out_1()

        if (self.xy_in_1 == None or self.xy_out_1 == None):
            print("Błędne  współrzędne. Podaj pozycje ponownie")

        else:
            self.cal_val_punch_1(self.xy_in_1[0], self.xy_in_1[1], self.xy_out_1[0], self.xy_out_1[1])
            if self.punch_1 == punch_max:
                self.set_trigger_1(self.xy_in_1[0], self.xy_in_1[1], self.xy_out_1[0], self.xy_out_1[1])
                self.set_n_2(self.xy_in_1[0], self.xy_in_1[1], self.xy_out_1[0], self.xy_out_1[1])
                self.change_xy_val_1(self.xy_in_1[0], self.xy_in_1[1], self.xy_out_1[0], self.xy_out_1[1])
            elif self.punch_1 != punch_max:
                print("Ruch niezgodny z zasadami. Podaj pozycje ponownie")
        return self.df_xy


# a = Move_1()
# b= a.set_n_2(1, 65, 2, 66)
# print (b)