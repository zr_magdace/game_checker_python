from Play import Play

def main():
    play = Play()
    play.run_game()
    play.set_result()

if __name__ == "__main__":
    main()

