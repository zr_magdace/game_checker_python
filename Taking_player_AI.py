from Move_player_AI import Move_player_AI

class Taking_player_AI(Move_player_AI):
    def __init__(self):
        super().__init__()

    def make_list_move_checker_2(self):
        xy_list = []
        for i in range(0, 8):
            for j in range(65, 73):
                val_in = super().read_xy_val(i, j)
                if val_in == 2:
                    for k in range(0, 8):
                        for m in range(65, 73):
                            val_out = super().read_xy_val(k, m)
                            if val_out == 3:
                                xy_list.append([i, j, k, m])
        #print(xy_list)
        return xy_list

    def make_list_move_king_2(self):
        xy_list = []
        for i in range(0, 8):
            for j in range(65, 73):
                val_in = super().read_xy_val(i, j)
                if val_in == 22:
                    for k in range(0, 8):
                        for m in range(65, 73):
                            val_out = super().read_xy_val(k, m)
                            if val_out == 3:
                                xy_list.append([i, j, k, m])
        return xy_list

    def make_xy_list_possible_move_2(self):
        xy_possible_move_lists = []
        a = self.make_list_move_checker_2()
        b = self.make_list_move_king_2()

        move_lists = super().make_list_of_possible_move_all_chacker(a, b)
        #print(f"move_list: {move_lists}")
        for i in range(len(move_lists)):
          c = move_lists[i]
          super().set_trigger_2(c[0], c[1], c[2], c[3])
          if self.trigger_2:
                xy_possible_move_lists.append(move_lists[i])
                self.trigger_2 = False
        #print(f"xy_possible _move_list: {xy_possible_move_lists}")
        return xy_possible_move_lists

    def cal_max_punch_2(self, xy_punch_lists):
        max_punch_list = []
        for i in range(len(xy_punch_lists)):
             a = xy_punch_lists[i]
             b = super().set_val_from_in_to_out(a[0], a[1], a[2], a[3])
             c = b[1:-1]
             max_punch_list.append(c.count(1) + c.count(11))
        if len(max_punch_list) > 0:
             max_punch_val = max(max_punch_list)
        else:
             max_punch_val = 0
        #print(f"max_punch_val:  {max_punch_val}")
        return max_punch_val

# punch = Punch_2()
# a = punch.make_xy_list_possible_move_2()
# b = punch.cal_max_punch_2(a)
# print(a)