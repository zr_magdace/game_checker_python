import pandas as pd

# xy = {65: [0, 1, 0, 3, 0, 2, 0, 2],  # ponizej wersja w petli
#     66: [1, 0, 1, 0, 22, 0, 3, 0],
#     67: [0, 3, 0, 3, 0, 1, 0, 2],
#     68: [1, 0, 1, 0, 3, 0, 3, 0],
#     69: [0, 3, 0, 3, 0, 2, 0, 2],
#     70: [1, 0, 3, 0, 1, 0, 2, 0],
#     71: [0, 1, 0, 3, 0, 3, 0, 2],
#     72: [1, 0, 1, 0, 3, 0, 2, 0]
#        }

# kodowanie pozycji:
# 0- pole niedzwolone, zeby pionek stanał
# 1- pole zajmowane przez pionek gracza (zwykly)
# 11- pole zajmowane przez pionek gracza (damka)
# 2 - pole zajmowane przez pionek AI
# 22 - pole zajmowane przez pionek AI (damka)
# 3 - pole niezajmowane przez pionek, dozowolone, zeby stanac

xy = {k: [0, 1, 0, 3, 0, 2, 0, 2] if k % 2 == 0 else [1, 0, 1, 0, 3, 0, 2, 0] for k in range(65, 73)}
df_xy_set = pd.DataFrame(xy)

class Move_players:
    def __init__(self):
        self.df_xy = df_xy_set
    def read_xy(self, Ln):
        try:
            a = len(Ln)
            y = ord(Ln[0].upper())
            x = int(Ln[1]) - 1
            if x in range(0, 8) and y in range(65, 73) and a == 2:
                return x, y
        except:
            return None
    def read_xy_val(self, x, y):
        try:
            xy_val = self.df_xy.at[x, y]
        except:
            xy_val = -1
        return int(xy_val)

    def cal_delta_ratio(self, x_in, y_in, x_out, y_out):
        delta_x = x_out - x_in
        delta_y = y_out - y_in
        abs_delta_x = abs(delta_x)
        abs_delta_y = abs(delta_y)
        if abs_delta_x == abs_delta_y:
            delta_ratio = 1
        else:
            delta_ratio = 0
        return delta_ratio

    def cal_val_xy_from_in_to_out(self, x_in, y_in, x_out, y_out): #wynik to lista wartości (start, miedzy, stop)
        val_list = []
        xy_list = []
        delta_x = x_out - x_in
        delta_y = y_out - y_in
        abs_delta_x = abs(delta_x)

        ratio_delta = self.cal_delta_ratio(x_in, y_in, x_out, y_out)
        if ratio_delta == 1:
            if x_out > x_in and y_out > y_in:
                for i in range (0, abs_delta_x+1):
                    a = x_in + i
                    b = y_in + i
                    val = self.read_xy_val(a, b)
                    val_list.append(val)
                    xy_list.append([a, b])
            elif  x_out > x_in and y_out < y_in:
                for i in range(0, abs_delta_x+1):
                        a = x_in + i
                        b = y_in - i
                        val = self.read_xy_val(a, b)
                        val_list.append(val)
                        xy_list.append([a, b])

            elif x_out < x_in and y_out < y_in:
                for i in range(0, abs_delta_x+1):
                        a = x_in - i
                        b = y_in - i
                        val = self.read_xy_val(a, b)
                        val_list.append(val)
                        xy_list.append([a, b])

            elif x_out < x_in and y_out > y_in:
                for i in range(0, abs_delta_x+1):
                        a = x_in - i
                        b = y_in + i
                        val = self.read_xy_val(a, b)
                        val_list.append(val)
                        xy_list.append([a, b])

        if ratio_delta == 0 and delta_x == 4 and delta_y == 0:
                for i in range(5):
                    x = x_in + i
                    y = y_in + [0, 1, 2, 1, 0][i]
                    val = self.read_xy_val(x, y)
                    val_list.append(val)
                    xy_list.append([x, y])

                for i in range(5):
                    x = x_in + i
                    y = y_in + [0, -1, -2, -1, 0][i]
                    val = self.read_xy_val(x, y)
                    val_list.append(val)
                    xy_list.append([x, y])

        if ratio_delta == 0 and delta_x == -4 and delta_y == 0:
                for i in range(5):
                    x = x_in - i
                    y = y_in + [0, 1, 2, 1, 0][i]
                    val = self.read_xy_val(x, y)
                    val_list.append(val)
                    xy_list.append([x, y])

                for i in range(5):
                    x = x_in - i
                    y = y_in + [0, -1, -2, -1, 0][i]
                    val = self.read_xy_val(x, y)
                    val_list.append(val)
                    xy_list.append([x, y])
        #print (val_list)
        return [xy_list, val_list]

    def set_xy_from_in_to_out(self, x_in, y_in, x_out, y_out):
        xy_val_list = self.cal_val_xy_from_in_to_out(x_in, y_in, x_out, y_out)
        return xy_val_list[0]

    def set_val_from_in_to_out(self, x_in, y_in, x_out, y_out):
        xy_val_list = self.cal_val_xy_from_in_to_out(x_in, y_in, x_out, y_out)
        return xy_val_list[1]

    def set_parameter_list(self, x_in, y_in, x_out, y_out):
        ratio_delta = self.cal_delta_ratio(x_in, y_in, x_out, y_out)
        delta_x = x_out - x_in
        val_list = self.set_val_from_in_to_out(x_in, y_in, x_out, y_out)
        return [ratio_delta, delta_x, val_list]

    def make_list_of_possible_move_all_chacker(self, a, b):
        move_list = a + b
        return move_list

# a=Move()
# b=a.cal_val_xy_from_in_to_out(2, 65, 7, 69)
# print (b[:])